/*
 * laser_sensor.cpp
 *
 *  Created on: Mar 29, 2019
 *      Author: Sag
        Note: c_period_callbacks just calls init, sensor_response,xshut & readRangeSingleMillimeters functions
                with address = 0x52;
 */

#include <stdio.h>
#include <stdbool.h>
#include "laser_sensor.h"
#include "i2c2.hpp"

bool i2c_init(unsigned int speedInKhz, uint8_t I2CAddr_Laser_sensor){
    bool ret_val =  I2C2::getInstance().init(speedInKhz);

    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x88,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = readReg_laser(I2CAddr_Laser_sensor,0x91);
    writeReg_laser(I2CAddr_Laser_sensor,0x91,stop_var);

    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x00);
    writeReg_laser(I2CAddr_Laser_sensor,0x00,0x01);
    if(ret_val) puts("laser sensor init\n");
    return ret_val;
}

bool check_sensor_response(uint8_t device_address){
    bool ret = I2C2::getInstance().checkDeviceResponse(device_address);
    //if (ret)   puts("device responds\n");
    return ret;
}

uint8_t readReg_laser(uint8_t device_address,uint8_t register_address){
    uint8_t ret = I2C2::getInstance().readReg(device_address,register_address);
    return ret;
}

bool writeReg_laser(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value){
    bool write = I2C2::getInstance().writeReg(deviceAddress,registerAddress,value);
    return write;
}

uint16_t readRangeSingleMillimeters(uint8_t I2CAddr_Laser_sensor){
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = readReg_laser(I2CAddr_Laser_sensor,0x91);
    writeReg_laser(I2CAddr_Laser_sensor,0x91,stop_var);

    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x00);
    writeReg_laser(I2CAddr_Laser_sensor,0x00,0x01);

    //uint16_t val1 = readReg_laser(I2CAddr_Laser_sensor,0x1E);
    uint16_t val2 = readReg_laser(I2CAddr_Laser_sensor,0x1F);
    //uint16_t val = (uint16_t) val1 << 8;
    //val |= (val2);
    return val2;
}
